<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>PHP</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style type="text/css">
		/* Show it is fixed to the top */
		body {
		  min-height: 75rem;
		  padding-top: 5.5rem;
		}
	</style>
</head>

<?php include "config/koneksi.php";
	$guests = mysqli_query($con, "SELECT * FROM regis");
?>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <a class="navbar-brand" href="#">REGISTRASI UAS</a>
	  </div>
	</nav>

	<div class="container">
	    <div class="row" >

			<div class="col-md-4">
				<div class="card">
			  		<div class="card-header">FORM</div>
		  			<form method="post" action="save_guest.php" role="form">
				  	<div class="card-body">
						<div class="form-group">
							<label for="title">title</label>
							<input type="text" class="form-control" id="title" name="title" placeholder="title*" required>
							<span class="help-block"><p class="help-block ">*wajib diisi</p></span>
						</div>

			            <div class="form-group">
			            	<label for="description">description</label>
						<div>
			            	<textarea class="form-control" type="textarea" id="description" name="description" placeholder="keterangan" maxlength="140" rows="7"></textarea>
			                <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
			            </div>
						</div>
						<div class="form-group">
			            	<div>
			            	    <label for="content">content</label>
			            	    <textarea class="form-control" type="textarea" id="content" name="content" placeholder="blabla" maxlength="140" rows="7"></textarea>
			                    <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
			                </div>
                        </div>
                        <form>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Example file input</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        </form>
			        </div>
				  	<div class="card-footer">
				  		<a href="javascript:location.reload(true)"  class="btn btn-light">Cancel</a>
				  		<button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Submit</button>
				  	</div>
			  		</form>
				</div>
               </div>
			</div>


			<div class="col-md-8">
				<div class="card">
				  <div class="card-header"> regis</div>
				  <div class="card-body">

					 <table class="table table-striped">
					    <thead>
					      <tr>
					        <th>title</th>
				            <th>description</th>
							<th>content</th>
							<th>image</th>
							<th>post_date</th>
				            <th>Action</th>
					      </tr>
					    </thead>

					    <tbody>
				        	<?php while ($g = mysqli_fetch_array($guests)){ ?>
					            <tr>
					                <td><?php echo $g['title'] ?></td>
									<td><?php echo $g['description'] ?></td>
                                    <td><?php echo $g['content'] ?></td>
                                    <td><?php echo $g['imag'] ?></td>
                                    <td><?php echo $g['post_date'] ?></td>
					                <td><button type="button" class="btn btn-secondary btn-sm" style="margin-right: 4px" data-toggle="modal"
                                    data-target="#edit-<?php echo $g['id'] ?>"><i class="glyphicon glyphicon-pencil"></i> Edit</button>

					                	<a href="delete_guest.php?user=<?php echo $g['id'] ?>"  class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah user ini akan di hapus ?');"><i class="glyphicon glyphicon-trash"></i> Del</a>

					            	</td>
					            </tr>

                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div id="edit-<?php echo $g['id']; ?>" class="modal" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <!-- Modal Header -->
                                      <div class="modal-header">
                                        <h4 class="modal-title">tambah</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      </div>

                                      <form  method="post" action="save_edit_guest.php?user= <?php echo $g['id'] ?>" role="form" >
                                      <div class="modal-body">
                                          	<div class="row">
                                          	    <div class="col-md-12">
                                          		<div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name*" value="<?php echo $g ['name']?> " required>

                                                </div>
                                                <div class="form-group">
                                                    <label for="address">Address</label>
                                                    <textarea class="form-control" type="textarea" id="address" name="address" placeholder="Alamat" maxlength="140" rows="7"><?php echo $g ['address']?></textarea>

                                                </div>
                                                <div class="form-group"> <?php echo $g ['jurusan']?>
                                                    <div>
                                                    	<label for="jurusan">jurusan</label></br>
                                                        <input type="radio" name="jurusan" value="IPA"
                                                        <?php if($g['jurusan']=='IPA') { echo 'checked'; }  ?>/>IPA </br>
                                                       <input type="radio" name="jurusan" value="IPS"
                                                        <?php if($g['jurusan']=='IPS') { echo 'checked'; }  ?>/>IPAS </br>
                                                    </div>
                                                </div>
                                                <div class="form-group"> <?php echo $g['Kelas'] ?>
                                                <div>
                                                <div>	<label for="Kelas">Kelas</label></br>
                                                        <input type="radio" name="kelas" value="10" <?php if($g['Kelas']=='10') { echo 'checked'; }  ?>/> 10
                                                        <input type="radio" name="kelas" value="11" <?php if($g['Kelas']=='11') { echo 'checked'; }  ?>/> 11
                                                </div>
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <div><label for="divisi">Divisi</label></br>
                                                        <input type="radio" name="div" value="syiar" <?php if($g['Divisi']=='syiar') { echo 'checked'; }  ?>/> Syiar</br>
                                                        <input type="radio" name="div" value="sba" <?php if($g['Divisi']=='sba') { echo 'checked'; }  ?>/> Seni Baca Al-QURAN</br>
                                                        <input type="radio" name="div" value="nasyid" <?php if($g['Divisi']=='nasyid') { echo 'checked'; }  ?>/> Nasyid</br>
                                                        <input type="radio" name="div" value="kaderisasi"<?php if($g['Divisi']=='kaderisasi') { echo 'checked'; }  ?>/> Kaderisasi</br>
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <div><label for="reason">Alasan</label>
                                                    <textarea class="form-control" type="textarea" id="reason" name="reason" placeholder="Karna.." maxlength="140" rows="7"><?php echo $g['Alasan'] ?></textarea>
                                                    <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
                                                    </div>
                                                </div>
                                                </div>
                                                </div>
                                            </div>
                                            <button type="submit" name="submit" class="btn btn-primary pull-right">Save</button>
                                            </form>
                                         </div>


                                          </div>
                                      </div>


				        	<?php } ?>
                            </tbody>

                     </table>
                  </div>

                </div>
            </div>
        </div>
    </div>




	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#characterLeft').text('140 characters left');
		    $('#address').keydown(function () {
		        var max = 140;
		        var len = $(this).val().length;
		        if (len >= max) {
		            $('#characterLeft').text('You have reached the limit');
		            $('#characterLeft').addClass('red');
		            $('#btnSubmit').addClass('disabled');
		        }
		        else {
		            var ch = max - len;
		            $('#characterLeft').text(ch + ' characters left');
		            $('#btnSubmit').removeClass('disabled');
		            $('#characterLeft').removeClass('red');
		        }
		    });

		} );
	</script>

</body>
</html>

