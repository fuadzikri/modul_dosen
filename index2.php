<!DOCTYPE html>
<html>
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>PHP</title>

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


	<!------ Include the above in your HEAD tag ---------->

	<style type="text/css">
		/* Show it is fixed to the top */
		.main{
		 	padding: 40px 0;
		}
		.main input,
		.main input::-webkit-input-placeholder {
		    font-size: 11px;
		    padding-top: 3px;
		}
		.main-center{
		 	margin-top: 30px;
		 	margin: 0 auto;
		 	max-width: 400px;
		    padding: 10px 40px;
			background:#009edf;
			    color: #FFF;
		    text-shadow: none;
			-webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
		-moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
		box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

		}
		span.input-group-addon i {
		    color: #009edf;
		    font-size: 17px;
		}

	</style>
</head>
<?php include "config/koneksi.php"; 
	$guests = mysqli_query($con, "SELECT * FROM guest");
?>
<body>

	<div class="container">

			<div class="row">
				<div class="col-md-4">

					<div class="main">
						<div class="main-center">

						<h5>Sign up once and watch any of our free demos.</h5>
							<form class="" method="post" action="save_guest.php">
								
								<div class="form-group">
									<label for="name">Your Name</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
											<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/>
									</div>
								</div>
								<button type="submit">SUBMIT</button>
							</form>

						</div>
					</div>
				</div>


				<div class="col-md-8">
					<div class="card">
					  <div class="card-header">
					  	List Users
					  </div>
					  <div class="card-body">
								
						 <table class="table table-striped">
						    <thead>
						      <tr>
						        <th>Nama</th>
					            <th>No HP</th>
					            <th>Alamat</th>
					            <th>Action</th>
						      </tr>
						    </thead>
						    <tbody>
					        	<?php while ($g = mysqli_fetch_array($guests)){ ?>
						            <tr>
						                <td width="10%"><?php echo $g['name'] ?></td>
						                <td width="10%"><?php echo $g['contact'] ?></td>
						                <td><?php echo $g['address'] ?></td>
						                <td><button type="button" class="btn btn-secondary btn-sm" style="margin-right: 4px" data-toggle="modal" data-target="#edit-<?php echo $g['id'] ?>"><i class="glyphicon glyphicon-pencil"></i> Edit</button>

						                	<a href="delete_guest.php?user=<?php echo $g['id'] ?>"  class="btn btn-danger btn-sm" onclick="return confirm('Apakah user ini akan di hapus ?');"><i class="glyphicon glyphicon-trash"></i> Del</a>
						         
						            	</td>
						            </tr>
									<!-- Trigger the modal with a button -->
									<!-- Modal -->
									<div id="edit-<?php echo $g['id']; ?>" class="modal" role="dialog">
									  <div class="modal-dialog">

									    <!-- Modal content-->
									    <div class="modal-content">
									      <!-- Modal Header -->
									      <div class="modal-header">
									        <h4 class="modal-title">Edit</h4>
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									      </div>

									      <form  method="post" action="save_edit_guest.php?user=<?php echo $g['id'] ?>" role="form">
										      <div class="modal-body">
										      	<div class="row">
										      		<div class="col-md-12">
									    				<div class="form-group">
									    					<label>Name</label>
															<input type="text" class="form-control" id="name" name="name" placeholder="Name*" value="<?php echo $g['name'] ?>" required>
															<span class="help-block"><p class="help-block ">*wajib diisi</p></span>
														</div>
														<div class="form-group">
									    					<label> Contact </label>
															<input type="text" class="form-control" id="mobile" name="contact" placeholder="08xxx" value="<?php echo $g['contact'] ?>">
														</div>
									                    <div class="form-group">
									    					<label> Address </label>
									                    	<textarea class="form-control" type="textarea" id="message_edit" name="address" placeholder="Alamat" maxlength="140" rows="7"> <?php echo $g['address'] ?> </textarea>        
									                    </div>
											    	</div>
											    </div>
										      </div>
										      <div class="modal-footer">
										        <button type="submit" name="submit" class="btn btn-primary pull-right">Save</button>
										      </div>
									      </form>
									    </div>
									  </div>
									</div>
					        	<?php } ?>
						    </tbody>
						  </table>
					  </div> 
					</div>
				</div>

			</div>

		</div><!--container-->

	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</body>
</html>